<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class OwnJob extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('own_job', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->string('job_name');
            $table->string('yawts');
            $table->text('tailbar');
            $table->text('tailbar_darga');
            $table->string('unelgee');
            $table->date('begin_date');
            $table->date('end_date');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('own_job');
    }
}
