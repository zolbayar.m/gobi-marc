<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facedes\DB;
use Carbon\Carbon;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB:table('users')->insert([
            [
                'name' => 'John Doe',
                'email' => 'Johndoe@gmail.com',
                'password' => Hash::make("john123"),
                'isAdmin' => '1',
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ],
            [
                'name' => 'Mark Doe',
                'email' => 'Markdoe@gmail.com',
                'password' => Hash::make("john123"),
                'isAdmin' => '0',
                'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
            ]
        ]);
    }
}
