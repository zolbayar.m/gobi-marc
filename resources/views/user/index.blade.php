@extends('layouts.app')
<head>
   <style>

</style>

</head>
@section('content')
<div class="container">
    <div class="row">
    <div class="col-sm-12">  
        <table class="table table-sm">
            <thead class="thead-light">
                <tr>
                <th class="row-0 row-date"><p class="sansserif">Огноо</p></th>
                <th scope="col" class="row-1 row-name"><p class="sansserif">Нэр</p></th>
                <th scope="col" class="row-2 row-job"><p class="sansserif">Хийгдэх ажил</p></th>
                
                <th scope="col" class="row-4 row-tailbar"><p class="sansserif">Ажилтны тайлбар</p></th>
                <th scope="col" class="row-3 row-tailbar_darga"><p class="sansserif">Дүгнэлт</p></th>
                <th scope="col" class="row-5 row-yawts"><p class="sansserif">Үйл явц</p></th>
                <th scope="col" class="row-7 row-enddate"><p class="sansserif">Дуусах хугацаа</p></th>
                <th scope="col" class="row-8 row-edit"><p class="sansserif">Засварлах</p></th>
                </tr>
            </thead>
            <tbody>
                @foreach($add as $item)
                <tr>
                <td scope="row"><p>{{date('M j',strtotime($item->created_at))}}</p></td>
                <th scope="row"><p>{{$item->users->name}}</p></th>
                <td class="table-head-breack"><div class="hehe"><p>{{$item->daalgwar}}</p></div></td>
                
                <td class="table-head-breack"><div class="hehe"><p>{{$item->tailbar}}</p></div></td>
                <td class="table-head-breack"><div class="hehe"><p>{{$item->tailbar_darga}}</p></div></td>
                <td>

                        
                     @if($item->yawts == 0)
                        
                      <p class="text-primary">Хийгдэж байгаа</p>
                      @elseif($item->yawts == 1)
                     <p class="text-success">Дууссан</p>

                    @else

                    @endif
                </td>
                <td><p>{{substr($item->enddate,0,10)}}</p></td>
                <td class="table-head-breack">
                        <a href='{{url("/uread/$item->id")}}' class="btn btn-primary btn-sm">Read</a>
                        <a href='{{url("/userupdate/$item->id")}}' class="btn btn-success btn-sm">Update</a>
                    </td>
                </tr>
                @endforeach
            </tbody>
            </table>
           
            <div class="pull-right">
                {{$add->links()}}
                </div>
        </div>
               
               
    </div>
    
</div>
@endsection
