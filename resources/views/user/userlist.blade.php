@extends('layouts.app')
<head>
<style>

/* #customers {
    font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
    border-collapse: collapse;
    width: 100%;
}

#customers td, #customers th {
    border: 1px solid #ddd;
    padding: 8px;
}

#customers tr:nth-child(even){background-color: #f2f2f2;}

#customers tr:hover {background-color: #ddd;} */
 
.users {
  table-layout: fixed;
  width: 100%;
  white-space: nowrap;
}
/* Column widths are based on these cells */
.row-name {
  width: 8%;
}
.row-job{
  width: 20%;
}
.row-yawts {
  width: 8%;
}
.row-tailbar {
  width: 15%;
  
}
.row-tailbar_darga {
  width: 15%;
  
}
.row-unelgee {
  width: 10%;
  
}
.row-enddate {
  width:5%;
  
}
.row-edit {
  width: 13%;
  
}
.users td {
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
}
.users th {
  background: darkblue;
  color: white;
}
.users td,
.users th {
  text-align: left;
  padding: 5px 10px;
}
.users tr:nth-child(even) {
  background: lightblue;
}

</style>
</head>
@section('content')
    <div class="container">
        <div class="row " style="margin-bottom:20px">
       
            <div class="col-sm-4">
            <form  action="{{route('ushow')}}" method="GET">
            <select class="form-control" id="exampleSelect1" name="name" style="margin-botton:50px">
                @foreach($user as $item)
                    <option value="{{ $item->id }}">{{ $item->name }}</option>
                @endforeach
            </select>
            
            
            </div>
            <div class="col-sm 4">
            <button type="submit" class="btn btn-primary">Хайх</button>
            </div>
            <div class="col-sm 4">
            </div>
            </form>
        
        </div>
      <div class="row">
      <div class="col-sm-0">
      
      </div>

        <div class="col-sm-12 table-responsive text-nowrap">

            <div class="col" style="margin-bottom: 20px;">
                <h3>Ажлын хүснэгт</h3>
                </div>

        <table class="table table-sm table-responsive">
            <thead class="thead-light">
            <tr>
                <th class="row-0 row-date"><p class="sansserif">Огноо</p></th>
                <th scope="col" class="row-1 row-name"><p class="sansserif">Нэр</p></th>
                <th scope="col" class="row-2 row-job"><p class="sansserif">Хийгдэх ажил</p></th>
                
                <th scope="col" class="row-4 row-tailbar"><p class="sansserif">Ажилтны тайлбар</p></th>
                <th scope="col" class="row-3 row-tailbar_darga"><p class="sansserif">Дүгнэлт</p></th>
                <th scope="col" class="row-5 row-yawts"><p class="sansserif">Үйл явц</p></th>
                <th scope="col" class="row-7 row-enddate"><p class="sansserif">Дуусах хугацаа</p></th>
                <th scope="col" class="row-8 row-edit"><p class="sansserif">Засварлах</p></th>
                </tr>
            </thead>
            <tbody>
                @foreach($add as $item)
                <tr>
                <td scope="row"><p>{{date('M j',strtotime($item->created_at))}}</p></td>
                <th scope="row"><p>{{$item->users->name}}</p></th>
                <td class="table-head-breack"><div class="hehe"><p>{{$item->daalgwar}}</p></div></td>
                
                <td class="table-head-breack"><div class="hehe"><p>{{$item->tailbar}}</p></div></td>
                <td class="table-head-breack"><div class="hehe"><p>{{$item->tailbar_darga}}</p></div></td>
                <td>

                        
                     @if($item->yawts == 0)
                        
                      <p class="text-primary">Хийгдэж байгаа</p>
                      @elseif($item->yawts == 1)
                     <p class="text-success">Дууссан</p>

                    @else

                    @endif
                </td>
                  
                  
                <td>{{date('M j',strtotime($item->enddate))}}</td>
                <td class="table-head-breack">
                    <a href='{{url("/uread/$item->id")}}' class="btn btn-primary btn-sm">Read</a>
                    <a href='{{url("/userupdate/$item->id")}}' class="btn btn-success btn-sm">Update</a>

                </td>
                </tr>
                @endforeach
            </tbody>
            </table>
            <div class="pull-right">
            {{$add->links()}}
            </div>
            
            
        </div>

            
    </div>
@endsection
