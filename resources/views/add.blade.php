@extends('layouts.app')
<head>
<style>

#customers {
    font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
    border-collapse: collapse;
    width: 100%;
}

#customers td, #customers th {
    border: 1px solid #ddd;
    padding: 8px;
}

#customers tr:nth-child(even){background-color: #f2f2f2;}

#customers tr:hover {background-color: #ddd;}
  
</style>
</head>
@section('content')
    <div class="container">
      <div class="row">
      
      @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
    @endif
    
        <div class="col-sm-6">

         <form method="post" action="{{route('add.job')}}"> {{ csrf_field() }}
        <div class="form-group">
        <label for="exampleSelect1"><h3>Ажил нэмэх</h3></label>
        <select class="form-control" id="exampleSelect1" name="name">
        @foreach($add as $item)
            <option value="{{ $item->id }}">{{ $item->name }}</option>
        @endforeach
        </select>
        </div>
        <div class="form-group">
            <label for="formGroupExampleInput">Хийгдэх ажил:</label>
            <textarea type="text" name="job" class="form-control" id="formGroupExampleInput" placeholder="Хийгдэх ажил"></textarea> 
        </div>
        <div class="form-group">
            <label for="formGroupExampleInput">Нэмэлт тайлбар:</label>
            <textarea type="text" name="tailbar_darga" class="form-control" id="formGroupExampleInput" placeholder="Тайлбар"></textarea> 
        </div>
        <div class="form-group">
            <label for="formGroupExampleInput2">Хийж дуусгах хугацаа:</label> <br>
            
            <input type="text" name="enddate" placeholder="YYYY-MM-DD" id="datepicker">
            

        </div>
        <div class="form=group">
                    <!-- <input type="submit" value="go" class="form-control"> -->
                    <button type="submit" class="btn btn-primary">Хадгалах</button>
          </div>
        </form>   
            
        </div>
    </div>

@endsection
