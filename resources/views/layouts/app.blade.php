<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- <title>{{ config('app.name', 'Маркетинг') }}</title> -->
    <title>Маркетинг 2018</title>

    <!-- Scripts -->


    <!-- Fonts -->
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-datetimepicker/2.5.4/jquery.datetimepicker.css">
    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{asset('css/main.css')}}" rel="stylesheet">
    <style>
table {
    font-family: SamsungSans, sans-serif;
    border-collapse: collapse;
    width: 100%;
}

td, th {
    border: 1px solid #dddddd;
    text-align: left;
    padding: 8px;
}

tr:nth-child(even) {
    /* background-color: #dddddd; */
}
</style>
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-expand-md navbar-light navbar-laravel">
            <div class="container">
                <a class="navbar-brand" href="#">
                    <!-- {{ config('app.name', 'Larave') }} -->
                    <h3></h3>Beta 1.0</h3>
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                   
                        <ul class="navbar-nav mr-auto">
                        <li class="nav-item active">
                        @if(Auth::user())
                        @if(Auth::user()->isAdmin == 1)
                            <a class="nav-link" href="{{url('/home')}}">Нүүр<span class="sr-only">(current)</span></a>
                        </li>
                        <li class="nav-item">
                                <a class="nav-link" href="{{url('add')}}">Ажил нэмэх<span class="sr-only">(current)</span></a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="{{url('list')}}">Хайх<span class="sr-only">(current)</span></a>
                            </li>
                            </ul>
                        @else
                             <li class="nav-item">
                                <a class="nav-link" href="{{url('users')}}">Нүүр<span class="sr-only">(current)</span></a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="{{url('useradd')}}">Ажил нэмэх<span class="sr-only">(current)</span></a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="{{url('userlist')}}">Хайх<span class="sr-only">(current)</span></a>
                            </li>
                            
                        @endif
                    @endif
                    </ul>
                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
                        @guest
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('login') }}">{{ __('Нэвтрэх') }}</a>
                            </li>
                            <li class="nav-item">
                                @if (Route::has('register'))
                                    <a class="nav-link" href="{{ route('register') }}">{{ __('Бүртгүүлэх') }}</a>
                                @endif
                            </li>
                        @else
                            <li class="nav-item dropdown" style="display:flex">
                                <span style="font-weight:bold;margin-top:3px;">{{Auth::user()->name}}</span>
                                <a class="dropdown-item" href="{{ route('logout') }}"

                                        
                                    
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                                   
                                        {{ __('Гарах') }}
                                        
                                    
                                    </a>


                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    
                                </div>
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>

        <main class="py-4">
            @yield('content')
        </main>


    </div>
<script src="https://code.jquery.com/jquery-2.2.4.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-datetimepicker/2.5.4/build/jquery.datetimepicker.full.js"></script>
  <script>



$('#datepicker').datetimepicker({
		format:	'Y-m-d H:i'
	});
 
</script>
   
</body>
</html>
