<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Add;
use App\User;
use Auth;
class NormalController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $add = Add::where('user_id',Auth::user()->id)->orderBy('id','DESC')->paginate(10);
       
        return view('user.index',compact('add'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function useradd(){
        $add = User::all();

        $add = Add::where('user_id',Auth::user()->id)->get();

        return view('user.useradd',compact('add'));

    }

    public function ustore(Request $request){
        $this->validate($request,[
            'job' => 'required',
            'tailbar' => 'required',
            'enddate' => 'required|date'
            
        ]);
        $job = new Add();
        $job->user_id = Auth::User()->id;
        $job->daalgwar = $request->job;
        $job->tailbar = $request->tailbar;
        $job->enddate = $request->enddate;
        $job->yawts = 0;
        $job->unelgee = 0;
        $job->save();
            return redirect('users');


    }
    public function ushow(Request $request){
        $number = $request->name;
        $data = Add::where('user_id',$number)->orderBy('id','DESC')->get();
        // var_dump($data);
        // die();
        // return view('user.ushow',compact('data'));



        $user = User::all();
        $add = Add::orderBy('id','DESC')->paginate(10);
        
        foreach($add as $item){
            $item->users;
        }
        
        // return Response::json($add);
        
        return view('user.ushow',compact('user','add','data'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = Add::find($id);
        return view('user.edit',compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function uread($id){
        $jobs = Add::find($id);
        return view('user.uread',compact('jobs'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $job = Add::find($id);

       
        $job->daalgwar = $request->job;
        $job->yawts = $request->yawts;
   
        $job->tailbar = $request->tailbar;
        $job->enddate = $request->enddate;
        $job->save();
        return redirect('users');
    }
    
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
