<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Add extends Model
{
    protected $table = 'jobs';
    

    public function users()
    {
        return $this->belongsTo('App\User','user_id');
    }


}
